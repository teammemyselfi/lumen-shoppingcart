<?php

use App\User;
use Illuminate\Http\Request;

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('user/balance', 'UserController@getUserBalance');
$router->get('cart', 'CartController@getCart');
$router->post('cart/addToCart', 'CartController@addToCart');
$router->post('cart/deleteItemToCart', 'CartController@deleteItemToCart');
$router->post('cart/updateCartItemQuantity', 'CartController@updateCartItemQuantity');
$router->post('cart/purchase', 'CartController@purchase');
$router->get('products', 'ProductController@getProducts');
$router->post('rating/updateRating', 'ProductRatingController@updateRating');

$router->get('/', 'HomeController@home');
