<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRating extends Model
{
    public $timestamps = false;

    public function store($productId, $rating)
    {
        return \DB::insert("INSERT INTO product_ratings(product_id, rating) VALUES(?, ?)", [$productId, $rating]);
    }
}
