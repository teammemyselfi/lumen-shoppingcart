<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;

    public function getProducts()
    {
        return $this::leftjoin("product_ratings", "products.id", "product_ratings.product_id")
                    ->select("products.*", "products.name", \DB::raw("AVG(product_ratings.rating) AS rating"))
                    ->groupBy("products.id")
                    ->get();
    }

    public function getProductById($id)
    {
        return $this::leftjoin("product_ratings", "products.id", "product_ratings.product_id")
                    ->select("products.*", "products.name", \DB::raw("AVG(product_ratings.rating) AS rating"))
                    ->groupBy("products.id")
                    ->where("products.id", "=", $id)
                    ->get();
    }
}
