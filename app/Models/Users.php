<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $timestamps = false;

    public function getUsers()
    {
        return $this::all();
    }

    public function getUserById($id)
    {
        return $this->findOrFail(1);

    }

    public function getUserBalance()
    {
        return $this->find(1)->balance;
    }

    public function updateBalance($newBalance)
    {
        $user = $this->find(1);

        $user->balance = $newBalance;

        $user->save();
    }
}
