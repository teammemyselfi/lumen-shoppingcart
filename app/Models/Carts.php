<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public $timestamps = false;

    protected $table = "cart";

    public function getCart()
    {
        return $this::join("products", "cart.product_id", "products.id")
                    ->select("cart.*", "products.name")
                    ->get();
    }

    public function getCartById($id)
    {
        return $this::join("products", "cart.product_id", "=", "products.id")
                    ->where("cart.id", "=", $id)
                    ->select("cart.*", "products.*")
                    ->get();
    }

    public function getCartByProductId($productId)
    {
        return $this::join("products", "cart.product_id", "=", "products.id")
                    ->where("cart.product_id", "=", $productId)
                    ->select("cart.*", "products.*")
                    ->get();
    }

    public function store($productId, $userId, $quantity, $total_price)
    {
        return \DB::insert("INSERT INTO cart(product_id, user_id, quantity, total_price) VALUES(?, ? ,?, ?)", [$productId, $userId, $quantity, $total_price]);
    }

    public function updateCart($productId, $quantity, $total_price)
    {
        return $this::where("cart.product_id", "=", $productId)
                    ->update(["quantity" => $quantity, "total_price" => $total_price]);
    }

    public function deleteCart($cartId)
    {
        return $this::destroy($cartId);
    }

    public function deleteAll()
    {
        return \DB::table("cart")->delete();
    }

    public function purchase($previous_balance, $total_purchase, $shipping_option_fee)
    {
        return \DB::insert("INSERT INTO purchases(previous_balance, total_purchase, shipping_option_fee) VALUES(?, ?, ?)", [$previous_balance, $total_purchase, $shipping_option_fee]);
    }
}
