<?php

namespace App\Http\Controllers;

class ProductController extends Controller
{
    protected $helpers;

    public function __construct(\App\Helpers\Helpers $helper)
    {
        $this->helpers = $helper;
    }

    public function getProducts()
    {
        $products = new \App\Product;

        return $this->helpers->JSONResponse(true, "", $products->getProducts());
    }
}
