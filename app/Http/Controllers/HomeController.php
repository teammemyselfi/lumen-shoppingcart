<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $helpers;

    public function __construct(\App\Helpers\Helpers $helper)
    {
        $this->helpers = $helper;
    }

    public function home(Request $request)
    {
        $user = new \App\User;
        $user->updateBalance(100.00);
        $request->session()->put("set_product_rating", []);

        return view("home");
    }
}
