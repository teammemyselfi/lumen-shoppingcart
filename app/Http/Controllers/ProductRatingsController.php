<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductRatingController extends Controller
{
    protected $helpers;

    public function __construct(\App\Helpers\Helpers $helper)
    {
        $this->helpers = $helper;
    }

    public function updateRating(Request $request)
    {
        $productId = (int)$request->input("product_id");
        $rating = (int)$request->input("rating");
        $set_product_rating = [];

        if ($request->session()->has("set_product_rating"))
        {
            $set_product_rating = $request->session()->get("set_product_rating");
        }

        if (in_array($productId, $set_product_rating) === true && $set_product_rating !== [])
        {
            return $this->helpers->JSONResponse(false, "You have already rated this product.", (new \App\Product)->getProductById($productId));
        }

        (new \App\ProductRating)->store($productId, $rating);

        array_push($set_product_rating, $productId);
        $request->session()->put("set_product_rating", $set_product_rating);

        return $this->helpers->JSONResponse(true, "", (new \App\Product)->getProductById($productId));
    }
}
