<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    protected $helpers;

    public function __construct(\App\Helpers\Helpers $helper)
    {
        $this->helpers = $helper;
    }

    public function getCart()
    {
        $cart = new \App\Cart;

        return $this->helpers->JSONResponse(true, "", $cart->getCart());
    }

    public function addToCart(Request $request)
    {
        $productId = $request->input("product_id");

        $cartModel = new \App\Cart;

        $cart = $cartModel->getCartByProductId($productId);
        $product = (new \App\Product)->getProductById($productId);

        if (count($cart) > 0)
        {
            $quantity = $cart[0]["quantity"] + 1;
            $total_price = $product[0]["price"] * $quantity;

            $cart = $cartModel->updateCart($product[0]["id"], $quantity, $total_price);
        }
        else
        {
            $user = (new \App\User)->getUsers();

            $cart = $cartModel->store($product[0]["id"], $user[0]["id"], 1, $product[0]["price"]);
        }

        return $this->helpers->JSONResponse(true, "", $cartModel->getCart());
    }

    public function deleteItemToCart(Request $request)
    {
        $cart = new \App\Cart;
        $cartId = $request->input("cart_id");

        $cart->deleteCart($cartId);

        return $this->helpers->JSONResponse(true, "", $cart->getCart());
    }

    public function updateCartItemQuantity(Request $request)
    {
        $productModel = new \App\Product;
        $cartModel = new \App\Cart;

        $cartId = $request->input("cart_id");
        $productId = $request->input("product_id");
        $quantity = $request->input("quantity");

        $product = $productModel->getProductById($productId);

        $total_price = floatval($product[0]["price"]) * $quantity;

        $cartModel->updateCart($productId, $quantity, $total_price);

        return $this->helpers->JSONResponse(true, "", $cartModel->getCart());
    }

    public function purchase(Request $request)
    {
        $userModel = new \App\User;
        $cartModel = new \App\Cart;

        $user = $userModel->getUsers();

        $total_purchase = (float) $request->input("total_purchase");
        $shipping_option_fee = (int) $request->input("shipping_option_fee");
        $previous_balance = (float) $user[0]["balance"];
        $new_balance = $previous_balance - $total_purchase - $shipping_option_fee;

        if ($new_balance < 0)
        {
            return $this->helpers->JSONResponse(false, "Warning", array("Current balance is not enough for this purchase."));
        }

        $cartItems = $cartModel->getCart();

        $cartModel->purchase($previous_balance, $total_purchase, $shipping_option_fee);

        $cartModel->deleteAll();

        $userModel->updateBalance($new_balance);

        return $this->helpers->JSONResponse(true, "", array("previous_bal" => $previous_balance, "total_purchase" => $total_purchase, "new_bal" => $new_balance, "shipping_option_fee" => $shipping_option_fee, "cart_items" => $cartItems));
    }
}
