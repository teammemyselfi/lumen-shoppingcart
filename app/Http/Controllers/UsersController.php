<?php

namespace App\Http\Controllers;

class UserController extends Controller
{
    protected $helpers;

    public function __construct(\App\Helpers\Helpers $helper)
    {
        $this->helpers = $helper;
    }

    public function getUserBalance()
    {
        $user = new \App\User;

        return $this->helpers->JSONResponse(true, "", $user->findOrFail(1)->balance);
    }
}
