<?php

namespace App\Helpers;

class Helpers
{
    function JSONResponse($Success = true, $Message = "", $data = null)
    {
        $obj = new \stdClass();

        $obj->Success 		= 	$Success;
        $obj->Message 		= 	$Message;
        $obj->Response 		= 	$data;

        return json_encode($obj);
    }
}
