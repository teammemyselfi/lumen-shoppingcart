$( document ).ready(function()
{
    getUserBalance();
    loadShoppingCart();
    loadProducts();

    $("#product_catalog").on("click", ".add_to_cart", function(event)
    {
        $.ajax(
        {
            url: 'cart/addToCart',
            type: "POST",
            dataType: "json",
            data: { "product_id": $(this).data("id") },
            error: function(){},
            success: function(data)
            {
                if (data.Success == true)
                {
                    InitializeShoppingCart(data.Response);
                }
            }
        });
    });

    $(".table-shopping-cart").on("click", ".remove-item", function()
    {
        $.ajax(
        {
            url: 'cart/deleteItemToCart',
            type: "POST",
            dataType: "json",
            data: { "cart_id": $(this).data("id") },
            error: function(){},
            success: function(data)
            {
                if (data.Success == true)
                {
                    InitializeShoppingCart(data.Response);
                }
            }
        });
    });

    $("#pay_now").click(function()
    {
        var total_price = parseFloat($("#total_price").html());
        var shipping_option = $("#shipping_option").val();

        if (total_price < 1)
        {
            modal_info("Cart is empty.");
        }
        else if (shipping_option == "")
        {
            modal_info("Please select a shipping option.");
        }
        // if no error and ready to purchase
        else
        {
            var data = {
                "total_purchase": total_price,
                "shipping_option_fee": shipping_option
            }

            $.ajax(
            {
                url: 'cart/purchase',
                type: "POST",
                dataType: "json",
                data: data,
                error: function(){},
                success: function(data)
                {
                    if (data.Success == true)
                    {
                        loadShoppingCart();
                        getUserBalance();
                        $("#shipping_option").val("");

                        var body = "<span style='font-size: 24px; font-weight: bold;'>Items</span><br /><br />";

                        body += "<table class='table'>";
                        body += "<tr>";
                        body +=     "<th>Name</th>";
                        body +=     "<th>Quantity</th>";
                        body +=     "<th>Total</th>";
                        body += "</tr>";

                        $.each (data.Response["cart_items"], function(index, value)
                        {
                            body += "<tr>";
                            body +=     "<td>" + capitalizeFirstLetter(value.name) + "</td>";
                            body +=     "<td>" + value.quantity + "</td>";
                            body +=     "<td class='text-right'>$ " + value.total_price + "</td>";
                            body += "</tr>";
                        });

                        body += "<tr><td>Total Amount</td><td>&nbsp;</td><td colspan='3' class='text-right'>$ " + parseFloat(data.Response["total_purchase"]).toFixed(2) + "</td></tr>";
                        body += "<tr><td colspan='3'>&nbsp;</td></tr>";
                        body += "<tr><td>Balance</td><td>&nbsp;</td><td colspan='3' class='text-right'>$ " + parseFloat(data.Response["previous_bal"]).toFixed(2) + "</td></tr>";
                        body += "<tr><td>Shipping Fee</td><td>&nbsp;</td><td colspan='3' class='text-right'>$ " + parseFloat(data.Response["shipping_option_fee"]).toFixed(2) + "</td></tr>";
                        body += "<tr><td>Total Amount</td><td>&nbsp;</td><td colspan='3' class='text-right'>$ " + parseFloat(data.Response["total_purchase"]).toFixed(2) + "</td></tr>";
                        body += "<tr><td>Current Balance</td><td>&nbsp;</td><td colspan='3' class='text-right font-weight-bold'>$ " + parseFloat(data.Response["new_bal"]).toFixed(2) + "</td></tr>";
                        body += "</table>";

                        modal_info(body);
                    }
                    else
                    {
                        modal_info(data.Response[0]);
                    }
                }
            });
        }
    });

    $(".table-shopping-cart").on("change", "#cart_item_quantity", function()
    {
        $.ajax(
        {
            url: 'cart/updateCartItemQuantity',
            type: "POST",
            dataType: "json",
            data: { "cart_id": $(this).data('id'), "quantity": $(this).val(), "product_id": $(this).data('product-id') },
            error: function(){},
            success: function(data)
            {
                if (data.Success == true)
                {
                    InitializeShoppingCart(data.Response);
                }
                else
                {
                    modal_info(data.Response[0]);
                }
            }
        });
    });

    function getUserBalance()
    {
        $.ajax(
        {
            url: 'user/balance',
            type: "GET",
            dataType: "json",
            error: function(){},
            success: function(data)
            {
                if (data.Success == false)
                {
                    modal_info(data.Response[0]);
                }

                $("#my_balance").html(parseFloat(data.Response).toFixed(2));
            }
        });
    }

    function ratingChange(productId, rating)
    {
        $.ajax(
        {
            url: 'rating/updateRating',
            type: "POST",
            dataType: "json",
            data: { "product_id": productId, "rating": rating },
            error: function(){},
            success: function(data)
            {
                if (data.Success)
                {
                    if (data.Response == [])
                    {
                        return;
                    }

                    // remove stars
                    $(".rating-" + data.Response[0]["id"]).html("");

                    // reinitialize rating of the selected product
                    $(".rating-" + data.Response[0]["id"]).stars(
                    {
                        value: data.Response[0]["rating"],
                        click: function(i)
                        {
                            ratingChange(data.Response[0]["id"], i);
                        }
                    });

                    $("#rating-value-" + data.Response[0]["id"]).html(parseFloat(data.Response[0]["rating"]).toFixed(2));
                }
                else
                {
                    modal_info(data.Message);

                    // remove stars
                    $(".rating-" + data.Response[0]["id"]).html("");

                    // reinitialize rating of the selected product
                    $(".rating-" + data.Response[0]["id"]).stars(
                    {
                        value: data.Response[0]["rating"],
                        click: function(i)
                        {
                            ratingChange(data.Response[0]["id"], i);
                        }
                    });

                    $("#rating-value-" + data.Response[0]["id"]).html(parseFloat(data.Response[0]["rating"]).toFixed(2));
                }
            }
        });
    }

    function modal_info(body)
    {
        $("#modal_info #modal_text").html(body);
        $("#modal_info").modal("show");
    }

    function InitializeShoppingCart(data)
    {
        $(".table-shopping-cart").find("tr:gt(0)").remove();
        var total_price = 0;

        $.each(data, function(index, value)
        {
            $tr = "<tr>";
            $tr += "<td>" + capitalizeFirstLetter(value.name) + "</td>";
            $tr += '<td><div class="col-3"><input id="cart_item_quantity" type="number" value="' + value.quantity + '" min="1" step="1" data-id="' + value.id + '" data-product-id="' + value.product_id + '"></div>';
            $tr += "</td>";
            $tr += "<td>" + value.total_price + "</td>";
            $tr += "<td><a href=\"javascript:void(0)\" class='remove-item' data-id='" + value.id + "'>Remove</a></td>";
            $tr += "</tr>";

            total_price += parseFloat(value.total_price);

            $(".table-shopping-cart").append($tr);
        });

        $("#total_price").html(total_price.toFixed(2));
        $("input[type='number']").InputSpinner({ groupClass: "input-group-sm", buttonsClass: "btn-secondary" });
    }

    function loadShoppingCart()
    {
        $.ajax(
        {
            url: 'cart',
            type: "GET",
            dataType: "json",
            error: function(){},
            success: function(data)
            {
                if (data.Success == true)
                {
                    InitializeShoppingCart(data.Response);
                }
            }
        });
    }

    function loadProducts()
    {
        $.ajax(
        {
            url: 'products',
            type: "GET",
            dataType: "json",
            error: function(){},
            success: function(data)
            {
                if (data.Success == true)
                {
                    $("#product_catalog").html("");
                    $products = "";

                    $.each(data.Response, function(index, val)
                    {
                        $rating = (val.rating === null ? 0 : val.rating)

                        $products = '<div class="card box-shadow">';
                        $products += '  <div class="card-header font-weight-bold">' + capitalizeFirstLetter(val.name) + '</div>';
                        $products += '  <img class="card-img-top" src="images/' + val.image + '" alt="Card image cap" style="width: 15rem;">';
                        $products += '  <div class="card-body">';
                        $products += '      <h1 class="card-title pricing-card-title">$' + val.price + '</small></h1>';
                        $products += '      <div class="rating-wrapper text-center mb-1"><div class="rating-' + val.id + '"></div></div>';
                        $products += '      <div class="text-center mb-1"><span id="rating-value-' + val.id + '">' + parseFloat($rating).toFixed(2) + '</span> stars</div>';
                        $products += '      <button type="button" class="add_to_cart btn btn-lg btn-block btn-outline-primary" data-id="' + val.id + '">Add to cart</button>';
                        $products += '  </div>';
                        $products += '</div>';

                        $("#product_catalog").append($products);

                        // initialize rating after products html is created
                        $(".rating-" + val.id).stars(
                        {
                            value: $rating,
                            click: function(i)
                            {
                                ratingChange(val.id, i);
                            }
                        });
                    });
                }
            }
        });
    }

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
});
