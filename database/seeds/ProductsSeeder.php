<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("products")->insert([
            [
                "name" => "apple",
                "price" => 0.30,
                "image" => "apple.png"
            ],
            [
                "name" => "beer",
                "price" => 2.00,
                "image" => "beer.png"
            ],
            [
                "name" => "water",
                "price" => 1.00,
                "image" => "water_bottle.png"
            ],
            [
                "name" => "cheese",
                "price" => 3.74,
                "image" => "cheese.png"
            ]
        ]);
    }
}
